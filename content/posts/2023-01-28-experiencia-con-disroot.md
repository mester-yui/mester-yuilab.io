+++
title = "Experiencia con los servicios de Disroot"
date = 2023-01-28
lastmod = 2023-06-24T12:55:39+02:00
tags = ["Disroot", "SoftwareLibre"]
draft = false
+++

{{< figure src="/images/disroot-logo.png" >}}

En primer lugar tenemos que saber.


## ¿Que es Disroot? {#que-es-disroot}

Disroot es un conjunto de servicios bastante curioso ya que usa herramientas software libre y código abierto.
Proporcionando por defecto:

-   Chat:A través del estándar **XMPP**

-   Correo electrónico: Proporciona un email `user@disroot.org`

-   Nube: A través de la plataforma auto alojada **Nextcloud**

-   Lufi: Un servicio de subida temporal de archivos cifrado,
    hace lo mismo que el ya extinto **Firefox Send**, no necesitas cuenta y los archivos se almacenan de forma cifrada.

-   Búsqueda: Un motor de búsqueda impulsado por **Searxng**

-   Akkoma: Disroot proporciona su propia versión de la plataforma del fediverso llamada **Akkoma**.[Link aqui](https://fe.disroot.org)

-   Llamadas: Provee un servicio de vídeo llamadas a través de la plataforma **Jitsi**

    Por supuesto disroot ofrece más servicios pero no los voy nombrar todos creo que haría el articulo un poco largo.


## ¿Es gratis?, ¿Es seguro? {#es-gratis-es-seguro}

A la primera pregunta si es completamente gratuito registrarse y usar los servicios pudiendo donar de forma opcional si te gusta el proyecto, por supuesto y dado a que es necesario dinero para la infraestructura física
ofrecen la posibilidad de aumentar el almacenamiento base de la nube y del correo a través de un pago por año.

A la pregunta de si es seguro lo es Disroot es mantenido por la fundación Disroot una entidad sin animo de lucro.Si es seguro


## Proyecto Howto Disroot {#proyecto-howto-disroot}

El proyecto **Howto Disroot** es un proyecto creado por los propios creadores de Disroot con el propósito especifico de proporcionar documentación de calidad y traducida a varios idiomas sobre sus varios servicios.


## Mi experiencia con estos servicios {#mi-experiencia-con-estos-servicios}

La experiencia es bastante buena en general la comunidad es muy agradable, y través del chat comunitario donde también están los administradores es posible dar tus sugerencias y por su puesto puedes traducir y/o mejorar los múltiples servicios del proyecto o incluso la web de una forma muy sencilla
eso incluye también la web del proyecto **Howto Disroot** por ejemplo para traducir la pagina del proyecto **Howto** puedes acceder al chat de **Howto Disroot** en `howto@chat.disroot.org` o enviar un email a [howto@disroot.org](mailto:howto@disroot.org) para obtener la autorización para enviar tu traducción al repositorio.

{{< figure src="/images/disroot-services.png" >}}

Me podeis encontrar en XMPP a traves de este codigo QR:

{{< figure src="/images/XMPP.png" >}}
