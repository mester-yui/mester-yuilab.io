+++
title = "¿Por que uso ArchLinux con KDE Plasma?"
date = 2022-12-17
lastmod = 2023-06-24T12:55:37+02:00
tags = ["KDEplasma", "ArchLinux"]
draft = false
+++

## Potencia y simplicidad {#potencia-y-simplicidad}

Arch Linux ofrece no solo gran potencia sino tambien una gran
simplicidad. Ademas de eso es un sistema extraordinariamente versatil lo
que nos lleva al segundo punto


## Versatilidad y ultimas versiones {#versatilidad-y-ultimas-versiones}

Arch Linux ofrece no solo las ultimas versiones de los programas lo cual
es un punto muy a favor para los que nos gusta tener las ultimas
versiones de los programas sino que ademas es muy util su gran
versatilidad dandole a los usuarios gran capacidad para modificarlo y
eso me gusta


## ¿Por que uso KDE plasma? {#por-que-uso-kde-plasma}

El motivo por el que utilizo KDE plasma es por su potencia y
flexibilidad eso unido a el buen ecosistema de aplicaciones unido a este
entorno de escritorio lo cual lo vuelve un entorno francamente atractivo
y es algo que me parece interesante Asi que bueno este es mi
personalizacion de KDE plasma:

{{< figure src="/images/KDE.png" >}}
