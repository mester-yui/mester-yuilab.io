+++
title = "Como instalar paru en ArchLinux"
date = 2022-12-28
lastmod = 2023-06-24T12:55:38+02:00
tags = ["ArchLinux"]
draft = false
+++

## ¿Que es paru? {#que-es-paru}

Paru es un programa para instalar o actualizar programas desde AUR o
desde los repositorios normales pero se centra en AUR

Bien, que ventajas puede tener sobre otras alternativas pues en primer
lugar esta escrito en **Rust** y es muy potente y tiene bastantes
funciones interesantes


## ¿Como instalar paru? {#como-instalar-paru}

Para instalar paru necesitamos el meta paquete **base-devel** necesario
tambien para instalar otros paquetes a traves de **AUR** y tambien el
paquete **git** para clonar el repositorio desde el que construimos el
paquete con los siguientes comandos

```sh
sudo pacman -S --needed base-devel git
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

Copias todo esto en la terminal y listo asi se ve paru al instalar un
programa

{{< figure src="/images/paru.png" >}}

Perfecto, un programa muy bueno lo recomiendo 100%
