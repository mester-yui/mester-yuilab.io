+++
title = "Como instalar Telegram en Emacs"
date = 2022-12-31
lastmod = 2023-06-24T12:55:39+02:00
tags = ["Emacs", "Telegram"]
draft = false
+++

En este articulo voy a explicar como instalar telegram en emacs a traves
del cliente [telega](https://github.com/zevlg/telega.el/).


## Como lo instalo {#como-lo-instalo}

`M-x package-install RET telega RET`

**M-x**: es la tecla al lado de derecho de **Alt Gr** segun la nomenclatura
de Emacs

**RET**: es la tecla **Enter** segun la nomenclatura de Emacs


## Dependencias {#dependencias}

Ahora para poder usar telega necesitamos **TDLib** una libreria para
construir clientes de Telegram

La instalaremos en ArchLinux con el paquete **libtd-dev** necesario para
la instalacion de la version minima de TDLib solicitada por telega

![](/images/libtd-dev.png)
_Paquete Libtd-dev en los repositorios AUR_


## Configuracion {#configuracion}

Esta es la configuracion en **Elisp** de telega:

```elisp
(use-package tracking
  :ensure t)

  (use-package telega
    :commands telega
    :defer t
    :custom
    (telega-use-tracking-for '(any pin unread))
    (telega-chat-use-markdown-formatting t)
    (telega-emoji-use-images t)
    (telega-completing-read-function #'selectrum-completing-read)
    (telega-msg-rainbow-title t)
    (telega-chat-fill-column 75)
    (telega-appindicator-mode t)
    (telega-notifications-mode t)
    (telega-filters-custom nil))

  (define-key global-map (kbd "C-c t") telega-prefix-map)
  (setq telega-server-libs-prefix "/usr")
```

Asi es el resultado final

{{< figure src="/images/telega.png" >}}
