+++
title = "Experiencia con la herramienta hugo"
date = 2023-01-20
lastmod = 2023-06-24T12:55:39+02:00
tags = ["hugo"]
draft = false
+++

{{< figure src="/images/640px-Logo_of_Hugo_the_static_website_generator.svg.png" >}}

Hola a todos este articulo trata sobre la herramienta _Hugo_ y mi experiencia usandola
junto con **ox-hugo**.

Tengo que admitir que al principio me costaba algunas imagenes que no funcionaban entre otras cosas pero con el paso del
tiempo voy arreglando los fallos mas tontos bueno el problema de la imagen lo he arreglado en cuanto me di cuenta de cual era la ruta por defecto para
las imágenes.


## Problemas varios {#problemas-varios}

Uno de los problemas más importantes fue con las imagenes que no me cargaban pero lo arregle cuando me di cuenta
de que el directorio que tenia que poner para añadirlas era `/images/nombre_imagen.*`
ahora uno de los problemas que espero tener solucionado para el momento de publicar este articulo es que me aparezca
el boton **Compartir en el fediverso**.Tambien tengo unos problemas con el logo del RSS lo cual me fastidia el feed pero espero arreglarlo.
Por algun motivo cuando pongo:

```toml
enableShareOnFediverse = true
random_thing = true
random_thing = false
```

Y esto es un ejemplo de fallos son fallos que estoy en proceso de resolver.


## Ventajas frente a org-static-blog {#ventajas-frente-a-org-static-blog}

Las principales ventajas frente a org-static-blog es que permite personalizarlo más facilmente además gracias a ox-hugo es
posible poner todos los artículos ordenados en un archivo `*.org` Estructurado de la siguiente forma:

{{< figure src="/images/org-hugo.png" >}}

Además hugo es bastante más sencillo para poner temas visuales bonitos sin mucho conocimiento y no necesitamos muchas farragadas editando el css. Lo cual es una ventaja
