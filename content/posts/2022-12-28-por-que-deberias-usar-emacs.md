+++
title = "Por que deberia usar emacs"
date = 2022-12-28
lastmod = 2023-06-24T12:55:38+02:00
tags = ["Emacs", "SoftwareLibre", "GNU"]
draft = false
+++

## ¿Que es Emacs? {#que-es-emacs}

{{< figure src="/images/1024px-EmacsIcon.svg.png" >}}

GNU Emacs orginalemente EMACS(Editor MACroS) era un editor pensado para
el lenguaje Lisp a traves de una version propia de Lisp llamada Emacs
Lisp o **Elisp** es posible configurar Emacs o incluso crear complementos
aunque gracias a la informacion por internet no necesitas saber
programar para hacer la configuracion configuracion.

Bien por que deberia usarlo


## Ecosistema Emacs {#ecosistema-emacs}

Emacs posee un enorme ecosistema de complementos entre los que hay
navegador, calendario, cliente de email, redes sociales(ej. cliente de
mastodon propio), cliente de telegram y mucho más, todo esto es posible
gracias a Emacs Lisp por ejemplo:

Cliente de mastodon:

{{< figure src="/images/mastodon-emacs.png" >}}


## Org Mode {#org-mode}

{{< figure src="/images/org-mode-unicorn.svg" >}}

Org mode es un modo de edicion creado para emacs aunque actualmente es
algo más parecido a Markdown y viene instalado en Emacs con una muy
buena integracion viene en este por defecto por defecto, la principal
ventaja de org mode es que es mas potente que markdown pero no es tan
complejo como _LATEX_. Org mode es ademas una poderosa herramienta de
organizacion personal permitiendo añadir tareas de la siguiente manera

`* TODO Nombre de la tarea`

La puedes cambiar a otro estado con el atajo **Ctrl c Ctrl t** o en la
nomenclatura de emacs &lt;C-c C-t&gt;

`* NEXT Nombre de la tarea`

`* DONE Nombre de la tarea`

Bien lo que veis arriba son los estados que OrgMode permite para las
tareas pero podeis cambiarlo desde la configuracion de Emacs. Emacs
tambien permite hacer un monton mas de cosas añadir codigo escribir
documentacion incluso puedes hacer que el codigo sea parte de esta
documentacion, organizar tareas, añadir imagenes bueno una infinidad de
cosas de hecho este articulo en si mismo esta escrito en Org Mode.

Aqui teneis un ejemplo de mi editando este mismo articulo con Emacs:

{{< figure src="/images/Yo-editando-este-articulo-en-emacs.png" >}}

Bueno si con esto os interesa podeis aprender mas sobre Emacs y OrgMode
en las webs oficiales de los respectivos proyectos.

[GNU
Emacs](https://www.gnu.org/savannah-checkouts/gnu/emacs/emacs.html)

[Org Mode](https://orgmode.org/)

Y aqui mas enlaces de referencia:

[Emacs en Wikipedia](https://es.wikipedia.org/wiki/Emacs)

[OrgMode en Wikipedia](https://es.wikipedia.org/wiki/Org-mode)

[Org Mode en el blog de
Lazaro](https://elblogdelazaro.org/tags/org-mode/)
